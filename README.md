# csv2json
A simple tool for converting CSV into JSON.

## How to use it
If the CSV file has only data (no headers):

```bash
csv2json << EOF
1,2,3
EOF
```

Output:
```json
["1","2","3"]
```

If the CSV has a header naming the columns, it can be used to name the values
in the output JSON.
```bash
csv2json --with-headers << EOF
id,name,description
1,John Due,A very popular person
2,"Mr ""T""",80's person
EOF
```

Output (in multi-line for readability):
```json
{
  "description": "A very popular person",
  "id": "1",
  "name": "John Due"
}
{
  "description": "80's person",
  "id": "2",
  "name": "Mr \"T\""
}
```


## Features:
- Handlers escaped values correctly, so values can contain _commas_ or _"_
  signs;
- Can be used with arbitrarily big files;
- Can use CVS header to name values in the output.
