package main

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
)

var withHeader bool

func main() {
	flag.BoolVar(&withHeader, "with-headers", false, "Use the first line as header; values on the first like must be unique.")
	flag.Parse()

	reader := csv.NewReader(os.Stdin)
	writer := json.NewEncoder(os.Stdout)

	if withHeader {
		convertWithHeaders(reader, writer)
	} else {
		convertNoHeaders(reader, writer)
	}
}

func convertNoHeaders(
	reader *csv.Reader,
	writer *json.Encoder,
) {
	for {
		record, err := reader.Read()
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}

			fmt.Fprintf(os.Stderr, "error reading CSV: %v", err)
			os.Exit(1)
		}

		if err := writer.Encode(record); err != nil {
			fmt.Fprintf(os.Stderr, "error writing JSON: %v", err)
			os.Exit(1)
		}
	}
}

func convertWithHeaders(
	reader *csv.Reader,
	writer *json.Encoder,
) {
	headers, err := reader.Read()
	if err != nil {
		if errors.Is(err, io.EOF) {
			return
		}

		fmt.Fprintf(os.Stderr, "error reading CSV header: %v", err)
		os.Exit(1)
	}

	obj := make(map[string]string, len(headers))

	for {
		record, err := reader.Read()
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}

			fmt.Fprintf(os.Stderr, "error reading CSV: %v", err)
			os.Exit(1)
		}

		for ix, v := range record {
			k := headers[ix]
			obj[k] = v
		}

		if err := writer.Encode(obj); err != nil {
			fmt.Fprintf(os.Stderr, "error writing JSON: %v", err)
			os.Exit(1)
		}
	}
}
